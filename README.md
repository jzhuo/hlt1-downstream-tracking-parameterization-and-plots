# HLT1 Downstream Tracking Parameterization And Plots


This is a project that provides Jupyter Notebooks that produces parameters used by the HLT1 downstream tracking algorithm and performance plots of the same algorithm.

## Contents:

- Tracking parameterization:
    - Magnet point
    - First slope correction
    - Searching tolerance for UT hits
- Ghost killing:
    - A single hidden layer neural network
- Performance plots:
    - Efficiencies
    - Ghost rates
    - Throughputs
  
 